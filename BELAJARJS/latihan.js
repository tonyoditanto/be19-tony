var a = 1;
var b = 2;
const c = 3;

if(a == 1) {
  a = 11;
  let b = 22;
  
  console.log(a);
  console.log(b);
  console.log(c);
}

console.log(a, b, c);

function inputCheck(angka) {
    let text = "";
    let output = "";
    if (angka <= 10 && angka >= 0) {
        text = "benar";
    } else {
        text = "salah";
    }

    for (let i = 0; i < 5; i++) {
        output += "Input yang anda masukkan " + text + "</br>";
    }
    
    document.getElementById('demo').innerHTML = output;
}