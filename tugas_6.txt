Workbook 1

1. Visual Studio Code adalah salah satu code editor yang dapat diintegrasikan dengan GIT. Sistem operasi apa saja yang dapat menggunakan Visual Studio Code? Linux, mac, windows
2. Command palette adalah kolom perintah yang memudahkan para developer untuk memberikan perintah di Visual Studio Code. Cara cepat yang dapat digunakan untuk menggunakan command palatte adalah? Cntrl + shift + p
3. Source control adalah salah satu menu di Visual Studio Code yang dapat menghubungkan antara proyek dengan version control termasuk GIT. Ada berapa pilihan yang disediakan oleh menu source control dan apa saja pilihan tersebut? Apabila baru membuka aplikasi visual studio Maka pada menu source code hanya akan ada dua menu yaitu clone repository and open folder, sedangkan apabila sudah membuka sebuah repository atau melakukan cloning project dari remote server, maka vs code akan menyediakan berbagai fitur pendukung git seperti, pull, push, clone, checkout, commit, changes, branch, remote, stash.
4. Pada saat menginstal GIT terdapat 3 path environment yang dapat dipilih, apa saja pilihan tersebut? 
    * Use Git from gith bash
    * Git from the command line and also from 3rd-party software
    * Use Git and optional Unix tools from the Command Prompt,
5. Salah satu fitur GIT yang dapat mengirimkan perubahan ke master branch dari remote repository yang berhubungan dengan direktori lokal adalah? Git: Push

Workbook 2

1. Terdapat beberapa tipe proyek yang dapat dibuat ketika pengembang ingin membuat proyek baru di GitLab, sebutkan! Projek public dan private
2. Apa nama ekstensi yang digunakan untuk Gradle di Visual Studio Code? Gradle Language Support
3. Ketika anda menjalankan sebuah Task configure dan memilih opsi other, maka akan muncul sebuah file. Apa nama file tersebut? tasks.json
4. Terdapat dua pilihan tipe logon yang dapat dipilih oleh pengembang ketika melakukan pemasangan Jenkins di Windows, sebutkan! 
    * Run service via LocalSystem dan 
    * Run via Local or domain user.
5. Ketika anda sudah berhasil melakukan instalasi Jenkins, ternyata Anda baru ingat jika port 8080 sudah dipakai oleh web server lain. Bagaimana cara untuk dapat mengubah port Jenkins tersebut? 
    * Masuk ke direktori instalasi Jenkins (by default berada di Program Files/Jenkins)
    * Buka file Jenkins.xml
    * Cari “—httpPort=8080” dan ganti dengan alamat port yang baru Misalnya 8090
    * Restart server Jenkins dengan perintah “Jenkins.exe restart”