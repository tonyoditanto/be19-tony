def nilai = 5
println "Tugas 1 - Menentukan bilangan positif/negatif"
if (nilai >= 0) {
    println "Angka " +nilai+ " adalah bilangan positif"
}else{
    println "Angka " +nilai+ " adalah bilangan negative"
}

println "\nTugas 2 - Menentukan bilangan ganjil/genap"
switch(nilai % 2 == 0) {
    case true:
        println "Angka " +nilai+ " adalah bilangan genap"
    break
    default:
        println "Angka " +nilai+ " adalah bilangan ganjil\n"
    break
}

println "Tugas 3 - Looping Segitiga\n"
for (i in 5..0){
    println i
    i--
}

println ""
for (i in 1..6){
    for (j in 1..i){
        print '*'
    }
    println ""
}

println ""
for (i in 1..6){
    for (j in 1..i){
        print j
    }
    println ""
}

println ""
for (i in 1..6){
    if (i<6){
        for (j in 1..6-i) {
            print " "
        }
    }
    for (k in 1..i) {
        print k
    }
    println ""
}



